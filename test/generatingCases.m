clc;
close all;
l = 8;
d = 1;
strSet = repmat(' ',[20 600]);
plantedMotif = cell(1,20);
strPlantedSet = cell(1,20);
size = zeros(1,20);
startPosition = zeros(1,20);
s = randseq(l); 
for i = 1:20   
    strSet(i,:) = randseq(600); 
    startPosition(1, i) = unidrnd(600);
    plantedMotif{1,i} = editMotif(s, l, d);
    size(1,i) = length(plantedMotif{1,i});
    endPosition = startPosition(1,i)+size(1,i);
    strPlanted = char(plantedMotif{1,i});
    iend = startPosition(1,i)-1;
    strPlantedSet{1,i} = [strSet(i,1:iend), strPlanted,...,
        strSet(i, endPosition:end)];
end

fileName = ['planted_l', num2str(l), '_d', num2str(d), '.txt'];
fid = fopen(fileName,'wt');
for i = 1:20
    fprintf(fid, '> %d Motif %s planted as %s at position %d. \n',...,
        i, s, plantedMotif{1,i}, startPosition(1, i));
    fprintf(fid, '%s\n', strPlantedSet{1,i});
%cout << "> " << i << " Motif " << motif << " planted as "
	% << mutatedMotif << " at position " << p << "." << endl;
end
fclose(fid);