function strModified = editMotif(s, l, d)
    strModified = s;  
    delta = unidrnd(d+1)-1;
    deletion = zeros(delta);
    
    if delta ~= 0  
    for i = 1:delta
        deletion(i) = unidrnd(length(strModified));
        strModified(deletion(i)) = '';
    end
    end
    
    alpha = unidrnd(d-delta+1)-1;
    insertion = zeros(alpha);
    
    if alpha ~= 0    
    for j = 1:alpha
        insertion(j) = unidrnd(length(strModified));
        insertChar = randseq(1);
        strModified = [strModified(1:insertion(j) - 1), insertChar,...,
            strModified(insertion(j) : end)];
    end
    end
    
    beta = d - delta - alpha;
    substitution = zeros(beta);
    if beta ~= 0   
    for k = 1:beta
        substitution(k) = unidrnd(length(strModified));
        replacedChar = strModified(substitution(k));
        while strcmp(replacedChar,strModified(substitution(k))) == 1
        replacedChar = randseq(1);
        end
        strModified(substitution(k)) = replacedChar;
    end
    end
end

