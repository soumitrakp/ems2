import os
import subprocess

testdir = "../test"
exe = "./ems"

configs_run     = [ (8,1), (9,2), (12,2), (11,3), (16,3) ] # (13, 4)]
configs_ordered = [ (8,1), (12,2), (16,3), (9, 2), (11, 3), (13, 4)]

solvers = [ "2p", "2", "2m" ]
threads = [ 16, 8, 4, 2, 1 ]

num_run = 4

def run_one_test(l, d, s, t):
    infile = testdir + "/planted_l%d_d%d.txt" % (l, d)
    golden = testdir + "/golden_l%d_d%d.txt" % (l, d)
    outfile = testdir + "/planted_l%d_d%d_ems%s_l%d_d%d.txt" % (l, d, s, l, d)
    cmd = exe + " -s %s -l %d -d %d -t %d %s" % (s, l, d, t, infile)
    cmd += " | grep 'Motifs found' | cut -d' ' -f7,10"
    output = subprocess.check_output(cmd, shell=True).rstrip().split(" ")
    if (os.system("diff -q " + outfile + " " + golden)):
        print "Error !!!!"
        exit(-1)
    return (float(output[0]), float(output[1]))

def run_multiple(l,d,s,t,n):
    total_time = 0.0
    total_mem  = 0.0
    for i in range(n):
        print "Running (%d, %d) solver=%s num_threads=%d run=%d ... " % (l, d, s, t, i+1),
        (secs, kb) = run_one_test(l,d,s,t)
        print secs, "s", kb, "KB"
        total_time += secs
        total_mem  += kb
    return (total_time/n, total_mem/n)

def print_time(t):
    if (t>=3600):
        return "%.2f h" % (t/3600)
    if (t>=60):
        return "%.2f m" % (t/60)
    return "%.2f s" % (t)

def print_mem(t):
    if (t>=1000000):
        return "%.2f GB" % (t/1000000)
    if (t>=1000):
        return "%.2f MB" % (t/1000)
    return "%.2f KB" % (t)

time = {}
mem = {}

for c in configs_ordered:
    (l, d) = c
    time[c] = {}
    mem[c] = {}
    for s in ["1", "2", "2p", "2m"]:
        time[c][s] = {}
        mem[c][s] = {}
        if (s == '2p'):
            for t in [1, 2, 4, 8, 16]:
                (time[c][s][t], mem[c][s][t]) = (0.0, 0.0)                       
        else:
            (time[c][s][1], mem[c][s][1]) = (0.0, 0.0)

for c in configs_run:
    (l, d) = c
    for s in solvers:
        if (s == '2p'):
            for t in threads:
                (time[c][s][t], mem[c][s][t]) = run_multiple(l, d, s, t, num_run)                    	
    	else:
            (time[c][s][1], mem[c][s][1]) = run_multiple(l, d, s, 1, num_run)

for c in configs_ordered:
    print "\\midrule\n%s & time &" % (str(c)),
    for s in ["1", "2", "2m"]:
        print "%s &" % print_time(time[c][s][1]),
    for t in [1, 2, 4, 8, 16]:
        print "%s &" % print_time(time[c]['2p'][t]),
    print "\\\\ \\cmidrule(l){2-10}"
    print "%s & memory &" % (str(c)),
    for s in ["1", "2", "2m"]:
        print "%s &" % print_mem(mem[c][s][1]),
    for t in [1, 2, 4, 8, 16]:
        print "%s &" % print_mem(mem[c]['2p'][t]),
    print "\\\\"
