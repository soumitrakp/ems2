﻿#include<iostream>
#include<fstream>
#include<string>
#include<cstring>
#include<cstdint>
#include<cmath>
#include<vector>
#include<fstream>
#include<ctime>
#include<algorithm>
#include "omp.h"

#include "utils.h"
#include "motif.hpp"

vector<Motif> curr_array;
vector<Motif> tmp_array;
int l, d;

std::string domain("ACGT");
size_t domain_size=4;


void gen_nbrhood3(std::string x, int start, int alpha) {
  //std::cout << x << ", start=" << start << ", alpha=" << alpha << "\n";
  int len = x.size();
  if (alpha > 0) {
    for (int j=start; j<len+1; j++) {
      for (unsigned int k=0; k<domain.size(); k++) {
        x.insert(j,1,k);
        gen_nbrhood3(x, j, alpha-1); 
        x.erase(j,1);
      }
    }
  } else {
    curr_array.push_back(Motif(x));
//    nbrs.push_back(x); // std::cout << x << std::endl;
  }
}

void gen_nbrhood2(std::string x, int start, int sigma, int alpha) {
  //std::cout << x << ", start=" << start << ", alpha=" << alpha << ", sigma=" << sigma << "\n";
  int len = x.size();
  if (sigma > 0) {
    for (int j=start; j<len; j++) {
      for (unsigned int k=0; k<domain.size(); k++) {
        if ((unsigned int)x[j] == k) continue;
        char t = x[j];
        x[j] = k;
        gen_nbrhood2(x, j+1, sigma-1, alpha); 
        x[j] = t;
      }
    }
  } else {
    gen_nbrhood3(x, 0, alpha);
  }
}

void gen_nbrhood(std::string x, int start, int delta, int sigma, int alpha) {
  //std::cout << x << ", start=" << start << ", delta=" << delta << ", alpha=" << alpha << ", sigma=" << sigma << "\n";
  int len = x.size();
  if (delta > 0) {
    for (int j=start; j<len; j++) {
      char t = x[j];
      x.erase(j,1);
      gen_nbrhood(x,j,delta-1,sigma,alpha);
      x.insert(j,1,t);
    }
  } else {
    gen_nbrhood2(x, 0, sigma, alpha);
  }
}

void gen_all(std::string x, int l, int d) {
  int m = x.size();
  for (int q=-d; q<=+d; q++) {
    int k = l+q;
    for (int delta = std::max(0,q); delta <= (d+q)/2; delta++) {
      int alpha = delta - q;
      for (int sigma=0; sigma <= d+q-2*delta; sigma++) {
        //std::cout << "d=" << d << ",l=" << l << ",delta=" << delta << ", alpha=" << alpha << ", sigma=" << sigma << std::endl;
        for (int i=0; i<m-k+1; i++) {
          std::string kmer = x.substr(i, k);
          //std::cout << "kmer = " << kmer << "\n";
          gen_nbrhood(kmer, 0, delta, sigma, alpha);
        }
      }
    }
  }
}
    void radix_sort(vector<Motif> & vCmbItem, vector<Motif> & vCmbTmp, uint64 compact_count)
    {
      uint64 bucket_count[4]; // use domain_size
      for(int i = 0; i < l; i++)
      {
        for (size_t bucket =0; bucket < domain_size; bucket++)
          bucket_count[bucket] = 0;
        for(uint64 j = 0; j < compact_count;  j++ )
        {
            uchar x = vCmbItem[j].get_2bits(2*i);
            bucket_count[x]++;
        }
        for (size_t bucket=1; bucket < domain_size; bucket++)
          bucket_count[bucket] += bucket_count[bucket-1];
        uint64 j = compact_count;
        while (j--)
        {
            uchar x = vCmbItem[j].get_2bits(2*i);
            vCmbTmp[--bucket_count[x]].set(vCmbItem[j]);
        }
        swap(vCmbItem, vCmbTmp);
      }
    }


int main(int argc, char **argv) {
  if (argc < 4) {
    std::cout << "Usage: " << argv[0] << " <input-sequence-file> <l> <d>" << std::endl;
    exit(0);
  }
  std::string input(argv[1]);
  l = atoi(argv[2]);
  d = atoi(argv[3]);
  std::cout << "l=" << l << ", d=" << d << std::endl;
  Reads reads;
  read_file(input.c_str(), reads);
  encodeStrings(reads, domain);

  
    double begin = omp_get_wtime();
    gen_all(reads[0], l, d);
    std::cout << "neighbours of sequence " << 0 << " = " << curr_array.size() << std::endl; 
    uint64_t compact_count = curr_array.size();
    tmp_array.resize(compact_count);
    radix_sort(curr_array, tmp_array, compact_count);
    /*
    sort( nbrs.begin(), nbrs.end() );
    */
    curr_array.erase( unique( curr_array.begin(), curr_array.end() ), curr_array.end() );
    std::cout << "unique neighbours of sequence " << 0 << " = " << curr_array.size() << std::endl; 
    double end = omp_get_wtime();
    double elapsed = end-begin;
    std::cout << "Time taken for generating nbrs and radix sort without star is " << elapsed << " sec " << std::endl; 
//    for (auto &i : nbrs) {
//      std::cout << i << std::endl;
//    }
  
}

