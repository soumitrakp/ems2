﻿#include<iostream>
#include<fstream>
#include<string>
#include<cstring>
#include<cstdint>
#include<cmath>
#include<vector>
#include<fstream>
#include<ctime>
#include<algorithm>
#include "omp.h"

#include "utils.h"
#include "motif.hpp"

vector<Motif> curr_array;
vector<Auxif> curr_aux_array;
vector<Motif> tmp_array;
vector<Auxif> tmp_aux_array;

std::string domain("ACGT");
std::string x, mark;
size_t domain_size = 4;
bool rightmost, leftmost;
uint64_t expanded_count;

int l, d;

    void gen_nbrhood3(int start, int alpha, int count) {
      int len = x.size();
      if (alpha > 0) {
        for (int j=start; j<len+1; j++) {
          if ((mark[j] == 'Y') || (mark[j] == 'Z')) continue;
          x.insert(j,1,domain_size);
          mark.insert(j,1,'N');
          gen_nbrhood3(j+1, alpha-1, 4*count);
          x.erase(j,1);
          mark.erase(j,1);
        }
      } else {
        curr_array.push_back(Motif(x));
        curr_aux_array.push_back(Auxif(x, domain_size));
        //cout << pr(x, domain_size) << endl;
        //cout << curr_array[curr_array.size()-1].get_kmer(l) << endl;
        //assert(curr_array.size() == curr_aux_array.size());
        //assert(compare(curr_array[curr_array.size()-1], curr_aux_array[curr_aux_array.size()-1], x, domain_size));
        expanded_count += count;
      }
    }

    void gen_nbrhood2(int start, bool stars_before, int sigma, int alpha, int count) {
      int len = x.size();
      if (sigma > 0) {
        for (int j=start; j<len; j++, stars_before=false) {
          if ((mark[j] == 'Y')) continue;
          char rr = mark[j+1];
          if (!rightmost && stars_before && (rr == 'Y')) continue;
          char t = x[j];
          char r = mark[j];
          x[j] = domain_size;
          mark[j] = 'Y';
          if (!leftmost && stars_before && (rr == 'N')) mark[j+1] = 'Z';
          gen_nbrhood2(stars_before, j+1, sigma-1, alpha, 4*count);
          x[j] = t;
          mark[j] = r;
          mark[j+1] = rr;
        }
      } else {
        gen_nbrhood3(0, alpha, count);
      }
    }

    void gen_nbrhood(int start, int end, int delta, int sigma, int alpha, int count) {
      if (delta > 0) {
        for (int j=start; j<end; j++) {
          char t = x[j];
          char rr = mark[j];
          x.erase(j,1);
          mark.erase(j,1);
          char r = mark[j];
          mark[j] = 'Y';
          gen_nbrhood(j, end-1, delta-1, sigma, alpha, count);
          x.insert(j,1,t);
          mark[j] = r;
          mark.insert(j,1, rr);
        }
      } else {
        gen_nbrhood2(0, true, sigma, alpha, count);
      }
    }


void gen_all(std::string seq, int l, int d) {
  int m = seq.size();
  for (int q=-d; q<=+d; q++) {
    int k = l+q;
    for (int delta = std::max(0,q); delta <= (d+q)/2; delta++) {
      int alpha = delta - q;
      for (int sigma=0; sigma <= d+q-2*delta; sigma++) {
        //std::cout << "d=" << d << ",l=" << l << ",delta=" << delta << ", alpha=" << alpha << ", sigma=" << sigma << std::endl;
        for (int i=0; i<m-k+1; i++) {
          x = seq.substr(i, k);
          //std::cout << "kmer = " << kmer << "\n";
          rightmost = (i+k>=m);
          leftmost = (i<=0);
        mark = std::string(k+1, 'N');
        int start;
        if (!leftmost) { mark[0] = 'Z'; }
        if (!rightmost) { mark[k] = 'Z'; start = 1;} else { start = 0;}
        gen_nbrhood(start, k, delta, sigma, alpha, 1);
        }
      }
    }
  }
}

    void radix_sort(vector<Motif> & vCmbItem, vector<Auxif> & vCmbItemAux, vector<Motif> & vCmbTmp, vector<Auxif> & vCmbTmpAux, uint64 compact_count)
    {
      uint64 bucket_count[4]; // use domain_size
      for(int i = 0; i < l; i++)
      {
        for (size_t bucket =0; bucket < domain_size; bucket++)
          bucket_count[bucket] = 0;
        for(uint64 j = 0; j < compact_count;  j++ )
        {
          uchar y = vCmbItemAux[j].get_bit(i);
          if (y) {
            for (size_t bucket =0; bucket < domain_size; bucket++)
              bucket_count[bucket]++;
          } else {
            uchar x = vCmbItem[j].get_2bits(2*i);
            bucket_count[x]++;
          }
        }
        for (size_t bucket=1; bucket < domain_size; bucket++)
          bucket_count[bucket] += bucket_count[bucket-1];
        uint64 j = compact_count;
        compact_count = bucket_count[domain_size-1];
        while (j--)
        {
          uchar y = vCmbItemAux[j].get_bit(i);
          if (y) {
            for (size_t bucket=0; bucket < domain_size; bucket++) {
              vCmbTmp[--bucket_count[bucket]].set(vCmbItem[j]);
              vCmbTmp[bucket_count[bucket]].set_2bits(bucket, 2*i);
              vCmbTmpAux[bucket_count[bucket]].set(vCmbItemAux[j]);
            }
          } else {
	      uchar x = vCmbItem[j].get_2bits(2*i);
              vCmbTmp[--bucket_count[x]].set(vCmbItem[j]);
              vCmbTmpAux[bucket_count[x]].set(vCmbItemAux[j]);
          }
        }
        swap(vCmbItem, vCmbTmp);
        swap(vCmbItemAux, vCmbTmpAux);
      }
    }

int main(int argc, char **argv) {
  if (argc < 4) {
    std::cout << "Usage: " << argv[0] << " <input-sequence-file> <l> <d>" << std::endl;
    exit(0);
  }
  std::string input(argv[1]);
  l = atoi(argv[2]);
  d = atoi(argv[3]);
  std::cout << "l=" << l << ", d=" << d << std::endl;
  Reads reads;
  read_file(input.c_str(), reads);
    encodeStrings(reads, domain);


    double begin = omp_get_wtime();
    curr_array.clear();
    expanded_count = 0;
    gen_all(reads[0], l, d);
    std::cout << "neighbours of sequence " << 0 << " = " << curr_array.size() << std::endl; 
    uint64_t compact_count = curr_array.size();
    curr_array.resize(expanded_count);
    curr_aux_array.resize(expanded_count);
    tmp_array.resize(expanded_count);
    tmp_aux_array.resize(expanded_count);
    radix_sort(curr_array, curr_aux_array, tmp_array, tmp_aux_array, compact_count);
    curr_array.erase( unique( curr_array.begin(), curr_array.end() ), curr_array.end() );
    std::cout << "unique neighbours of sequence " << 0 << " = " << curr_array.size() << std::endl; 
    double end = omp_get_wtime();
    double elapsed = end-begin;
    std::cout << "Time taken for generating nbrs and radix sort with star is " << elapsed << " sec " << std::endl; 

//    for (auto &i : nbrs) {
//      std::cout << i << std::endl;
//    }
}

