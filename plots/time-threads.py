import os
import sys
import time
import subprocess as sp
import numpy

import matplotlib as mpl
mpl.use("pgf")
pgf_with_pdflatex = {
    "pgf.texsystem": "pdflatex",
    "font.family": "serif", # use serif/main font for text elements
    "font.size": 12,        # use serif/main font for text elements
    "text.usetex": True,    # use inline math for ticks
    "pgf.rcfonts": False,   # don't setup fonts from rc parameters
    "pgf.preamble": [
#         r"\usepackage[utf8x]{inputenc}",
#         r"\usepackage[T1]{fontenc}",
#         r"\usepackage{cmbright}",
         r"\usepackage{times}",
         ]
}
mpl.rcParams.update(pgf_with_pdflatex)
import matplotlib.pyplot as plt

configs = [(8,1), (9,2), (12,2), (11,3), (16, 3), (13,4)]


def plot(yname, ylabel, xlabel, ycolid, xcolid, keypos):
    plt.figure(num=None, figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel) 
    plt.tight_layout(1.25)
    plt.grid(b=True, which='both', color='0.15')
    plots = []
    for c in configs:
      (l, d) = c
      cfg = "(%d, %d)" % (l, d)
      dat = "res_l%d_d%d.txt" % (l, d)
      x, yAbs = numpy.loadtxt(dat, comments="#", usecols=[xcolid-1, ycolid-1], delimiter=' ', unpack=True)
      y = yAbs/yAbs[0]
      #print x,y
      line, = plt.plot(x, y, label = cfg, linewidth=4)
      plots.append(line)
    plt.legend(loc=keypos)
    plt.savefig(yname+".pdf")

plot("time-threads", "Scaling factor (time taken by $p$ threads / time taken by 1 thread)", "Number of threads ($p$)", 2, 1, 1) # 'right top'

