#set term png enhanced size 800,600
#set output 'histo.png'

set term tikz standalone
set output "motifhisto.tex"

set key off
set border 3
set xrange[0:30]
set yrange[0:970000]
set grid

set macro
total(fname) = sprintf("awk 'BEGIN{total=0;}{total += $1;}END{print total}' %s", fname)
uniq(fname) = sprintf("wc -l < %s" , fname)

# Each bar is half the (visual) width of its x-range.
set boxwidth 0.5 absolute
set style fill solid 1.0 noborder

bin_width = 0.1;
bin_number(x) = floor(x/bin_width)
rounded(x) = bin_width * ( bin_number(x) + 0.5 )

set xlabel 'Compact motif repetition count ($f$)'
set ylabel 'Number of distinct motifs repeated $f$ times'

set size 1,1
set multiplot
set bmargin at screen 0.1

set title "Without skipping"
set origin 0,0
set size 0.55,1.0
set lmargin at screen 0.1
set arrow from 3.6318,0 to 3.6318,400000 nohead lc rgb 'blue'
set label "Total: 4839690" at 10,850000
set label "Unique: 1332588" at 10,800000
set label "Avg repeat: 3.63" at 10,750000
plot 'without-rules.dat' using (rounded($1)):(1) smooth freq with boxes 
unset arrow
unset label
unset ylabel
unset xlabel
set ytics format " " 

set title "Skipping using rules"
set origin 0.55,0
set size 0.45,1.0
set lmargin at screen 0.55
set arrow from 1.55,0 to 1.55,970000 nohead lc rgb 'blue'
set label "Total: ".system(total('with-rules.dat')) at 10,850000
set label "Unique: ".system(uniq('with-rules.dat')) at 10,800000
set label "Avg repeat: 1.55" at 10,750000 # compute avg on the fly
plot 'with-rules.dat' using (rounded($1)):(1) smooth freq with boxes linecolor rgb "#00FF00" 

unset multiplot

